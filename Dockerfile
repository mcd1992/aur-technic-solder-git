# Technic Solder CentOS7 container
# docker build --pull -t username/tag:version .
# docker run -d --restart=always -v /srv/solder/:/srv/solder/ -p 127.0.0.1:8080:8080 username/tag:version
# http://docs.solder.io/docs/getting-started

FROM centos:7
MAINTAINER mcd1992

# Make port 8080 available for listening (must use -p for publishing)
EXPOSE 8080

# Update to latest packages from repos and install packages
ENV BASE_INSTALLS="epel-release sudo wget git vim bash-completion tmux tree unzip"
ENV XTRA_INSTALLS="composer nginx php php-fpm php-mcrypt supervisor"
RUN yum install -y $BASE_INSTALLS && yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm &&\
  yum-config-manager --enable remi-php70 && yum update -y && yum install -y $XTRA_INSTALLS

# Create screen-like tmux config file
RUN echo -e "unbind C-b\nset -g prefix C-a\nbind C-a send-prefix" > /etc/tmux.conf

# Create users/groups and cleanup some files
RUN useradd -d /home/sudouser -G wheel -m -U sudouser &&\
  echo "%wheel ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/wheelgroup &&\
  groupadd -g 6969 solder && useradd -d /srv/solder -m -g 6969 -u 6969 -s /sbin/nologin solder &&\
  usermod -aG solder nginx && rm /etc/php-fpm.d/www.conf

# Add config files
ADD nginx.conf /etc/nginx/nginx.conf
ADD solder-vhost.conf /etc/nginx/sites-enabled/solder-vhost.conf
ADD start.sh /start.sh
ADD supervisord.conf /etc/supervisord.conf
ADD php-fpm.conf /etc/php-fpm.conf

# Switch to solder user and setup solder
# If using bind mounts you need to do this manually
# USER solder
# RUN git clone https://github.com/TechnicPack/TechnicSolder.git /srv/solder/www
# WORKDIR /srv/solder/www
# RUN composer install --no-interaction && composer install --no-interaction

CMD ["/start.sh"]
